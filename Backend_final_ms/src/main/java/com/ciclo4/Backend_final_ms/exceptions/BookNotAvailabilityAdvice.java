package com.ciclo4.Backend_final_ms.exceptions;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@ResponseBody
public class BookNotAvailabilityAdvice {

    @ResponseBody
    @ExceptionHandler(BookNotAvailabilityException.class)
    @ResponseStatus(HttpStatus.CONFLICT)
    String EntityNotFoundAdvice(BookNotAvailabilityException ex){
        return ex.getMessage();
    }
}
