package com.ciclo4.Backend_final_ms.exceptions;

public class BookNotAvailabilityException extends RuntimeException {
    public BookNotAvailabilityException(String message){
        super(message);
    }
}
