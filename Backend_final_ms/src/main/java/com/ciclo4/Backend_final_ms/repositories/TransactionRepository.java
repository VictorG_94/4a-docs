package com.ciclo4.Backend_final_ms.repositories;
import com.ciclo4.Backend_final_ms.models.Transaction;
import org.springframework.data.mongodb.repository.MongoRepository;
import java.util.List;

public interface TransactionRepository extends MongoRepository<Transaction, String> {
    List<Transaction> findByUsernameOrigin(String usernameOrigin);
    List<Transaction> findByTypeLoan (String typeLoan);
    List<Transaction> findByBookLoan (String bookLoan);
}