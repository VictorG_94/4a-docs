package com.ciclo4.Backend_final_ms.repositories;
import com.ciclo4.Backend_final_ms.models.Account;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface AccountRepository extends MongoRepository<Account, String> {
}
