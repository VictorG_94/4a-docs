package com.ciclo4.Backend_final_ms.repositories;
import com.ciclo4.Backend_final_ms.models.Book;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface BookRepository extends MongoRepository<Book, String> {
}
