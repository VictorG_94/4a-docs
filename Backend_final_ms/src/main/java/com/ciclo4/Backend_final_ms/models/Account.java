package com.ciclo4.Backend_final_ms.models;
import org.springframework.data.annotation.Id;
import java.util.Date;

public class Account {
    @Id
    private String username;
    private Date lastChange;
    private String isActive;

    public Account(String username, Date lastChange, String isActive){
        this.username = username;
        this.lastChange = lastChange;
        this.isActive = isActive;

    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public Date getLastChange() {
        return lastChange;
    }

    public void setLastChange(Date lastChange) {
        this.lastChange = lastChange;
    }

    public String getIsActive() {
        return isActive;
    }

    public void setIsActive(String isActive) {
        this.isActive = isActive;
    }
}