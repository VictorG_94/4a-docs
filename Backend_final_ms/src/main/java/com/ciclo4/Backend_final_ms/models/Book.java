package com.ciclo4.Backend_final_ms.models;
import org.springframework.data.annotation.Id;

public class Book {
    @Id
    private String bookName;
    private String bookAvailability;
    private String note;

    public Book(String bookName, String bookAvailability, String note){
        this.bookName = bookName;
        this.bookAvailability = bookAvailability;
        this.note = note;

    }

    public String getBookName() {
        return bookName;
    }

    public void setBookName(String bookName) {
        this.bookName = bookName;
    }

    public String getBookAvailability() {
        return bookAvailability;
    }

    public void setBookAvailability(String bookAvailability) {
        this.bookAvailability = bookAvailability;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }
}