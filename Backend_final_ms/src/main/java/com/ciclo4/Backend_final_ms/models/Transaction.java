package com.ciclo4.Backend_final_ms.models;

import java.time.LocalDate;
import java.util.Date;

public class Transaction {
    private String id;
    private String usernameOrigin;
    private String typeLoan;
    private String bookLoan;
    private Date hourLoan;

    public Transaction(String id, String usernameOrigin, String typeLoan, String bookLoan, Date hourLoan){
        this.id = id;
        this.usernameOrigin = usernameOrigin;
        this.typeLoan = typeLoan;
        this.bookLoan = bookLoan;
        this.hourLoan = hourLoan;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsernameOrigin() {
        return usernameOrigin;
    }

    public void setUsernameOrigin(String usernameOrigin) {
        this.usernameOrigin = usernameOrigin;
    }

    public String getTipeLoan() {
        return getTypeLoan();
    }

    public void setTipeLoan(String tipeLoan) {
        this.setTypeLoan(tipeLoan);
    }

    public String getBookLoan() {
        return bookLoan;
    }

    public void setBookLoan(String bookLoan) {
        this.bookLoan = bookLoan;
    }

    public Date getHourLoan() {
        return hourLoan;
    }

    public void setHourLoan(Date hourLoan) {
        this.hourLoan = hourLoan;
    }

    public String getTypeLoan() {
        return typeLoan;
    }

    public void setTypeLoan(String typeLoan) {
        this.typeLoan = typeLoan;
    }

}