package com.ciclo4.Backend_final_ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BackendFinalMsApplication {

	public static void main(String[] args) {
		SpringApplication.run(BackendFinalMsApplication.class, args);
	}

}
