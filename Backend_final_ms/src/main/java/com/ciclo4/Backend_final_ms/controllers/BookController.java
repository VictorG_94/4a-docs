package com.ciclo4.Backend_final_ms.controllers;
import com.ciclo4.Backend_final_ms.models.Book;
import com.ciclo4.Backend_final_ms.repositories.BookRepository;
import com.ciclo4.Backend_final_ms.exceptions.BookNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
public class BookController{
    private final BookRepository bookRepository;

    public BookController(BookRepository bookRepository){
        this.bookRepository = bookRepository;
    }

    @GetMapping("/reserves/{bookName}")
    Book getBook(@PathVariable String bookName){
        return bookRepository.findById(bookName)
                .orElseThrow(() -> new BookNotFoundException("No está el libro solicitado"));
    }

    @PostMapping("/reserves")
    Book newBook (@RequestBody Book book){
        return bookRepository.save( book );

    }

    @PostMapping("/reserves/update")
    Book updateBook (@RequestBody Book book){
        Book update_Book = bookRepository.findById(book.getBookName()).orElse(null);

        if (update_Book == null){
            throw new BookNotFoundException("No se encontró el libro solicitado. Por favor, confirmar la existencia del libro o contactar a la división de bibliotecas");
        }
        update_Book.setBookAvailability(book.getBookAvailability());
        bookRepository.save(update_Book);
        return update_Book;
    }
}