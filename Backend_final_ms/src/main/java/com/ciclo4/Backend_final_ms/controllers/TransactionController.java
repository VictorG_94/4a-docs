package com.ciclo4.Backend_final_ms.controllers;
import com.ciclo4.Backend_final_ms.exceptions.TransactionNotFoundException;
import com.ciclo4.Backend_final_ms.models.Account;
import com.ciclo4.Backend_final_ms.models.Book;
import com.ciclo4.Backend_final_ms.models.Transaction;
import com.ciclo4.Backend_final_ms.repositories.AccountRepository;
import com.ciclo4.Backend_final_ms.repositories.TransactionRepository;
import com.ciclo4.Backend_final_ms.repositories.BookRepository;
import com.ciclo4.Backend_final_ms.exceptions.AccountNotFoundException;
import com.ciclo4.Backend_final_ms.exceptions.BookNotFoundException;
import com.ciclo4.Backend_final_ms.exceptions.BookNotAvailabilityException;
import org.springframework.web.bind.annotation.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@RestController
public class TransactionController {
    private final TransactionRepository transactionRepository;
    private final AccountRepository accountRepository;
    private final BookRepository bookRepository;

    public TransactionController(AccountRepository accountRepository, TransactionRepository transactionRepository, BookRepository bookRepository) {
        this.transactionRepository = transactionRepository;
        this.accountRepository = accountRepository;
        this.bookRepository = bookRepository;
    }

    @PostMapping("/transactions")
    Transaction newTransaction(@RequestBody Transaction transaction) {
        Account accountOrigin = accountRepository.findById(transaction.getUsernameOrigin()).orElse(null);
        Book bookValidate = bookRepository.findById(transaction.getBookLoan()).orElse(null);

        if (accountOrigin == null) {
            throw new AccountNotFoundException("No se encontró una cuenta asociada al usuario" + transaction.getUsernameOrigin() + ". Intente nuevamente");
        }

        if (bookValidate == null) {
            throw new BookNotFoundException("No se encontró el libro solicitado. Por favor, confirmar la existencia del libro o contactar a la división de bibliotecas");
        }


        if (Objects.equals(bookValidate.getBookAvailability(), "No")) {
            throw new BookNotAvailabilityException("El libro no esta disponible actualmente");
        }

        bookValidate.setBookAvailability("No");
        accountOrigin.setLastChange(new Date());
        accountRepository.save(accountOrigin);
        bookRepository.save(bookValidate);

        transaction.setHourLoan(new Date());
        return transactionRepository.save(transaction);

    }

    @GetMapping("/transactions/{username}")
    List<Transaction> userTransaction(@PathVariable String username) {
        Account userAccount = accountRepository.findById(username).orElse(null);
        if (userAccount == null) {
            throw new AccountNotFoundException("No se encontró una cuenta asociada al usuario" + username + "intente nuevamente");
        }

        List<Transaction> transactionsOrigin = transactionRepository.findByUsernameOrigin(username);
        List<Transaction> transactionsBook = transactionRepository.findByBookLoan(username);
        List<Transaction> transactionsType = transactionRepository.findByTypeLoan(username);

        List<Transaction> transactions_full = Stream.concat(transactionsBook.stream(), transactionsOrigin.stream()).collect(Collectors.toList());
        return transactions_full;

    }
}
