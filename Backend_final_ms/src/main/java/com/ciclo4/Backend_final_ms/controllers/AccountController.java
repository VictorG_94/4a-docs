package com.ciclo4.Backend_final_ms.controllers;
import com.ciclo4.Backend_final_ms.models.Account;
import com.ciclo4.Backend_final_ms.repositories.AccountRepository;
import com.ciclo4.Backend_final_ms.exceptions.AccountNotFoundException;
import org.springframework.web.bind.annotation.*;

@RestController
public class AccountController {

    private final AccountRepository accountRepository;

    public AccountController (AccountRepository accountRepository){
        this.accountRepository = accountRepository;
    }

    @GetMapping ("/")
    String messageRoot(){
        return "Bienvenido a Accounts";
    }

    @GetMapping("/accounts/{username}")
    Account getAccount(@PathVariable String username){
        return  accountRepository.findById(username)
                .orElseThrow(() -> new AccountNotFoundException("No se encontró al usuario"));
    }

    @PostMapping("/accounts")
    Account newAccount(@RequestBody Account account){
        return accountRepository.save( account );
    }
}
